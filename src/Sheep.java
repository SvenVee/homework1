
public class Sheep {

   enum Animal {sheep, goat};
   
   public static void main (String[] param) {
      Animal loomad[] = new Animal[]{
              Animal.goat,
              Animal.sheep,
              Animal.goat,
              Animal.sheep,
              Animal.sheep,
              Animal.goat,
              Animal.goat,
              Animal.goat
      };
      reorder(loomad);
   }
   
   public static void reorder (Animal[] animals) {
      int sheepCounter = 0;
      int goatCounter = 0;

      //sheep and goat counter
      for (int i = 0; i<animals.length; i++){
         if (animals[i].equals(Animal.sheep)){
            sheepCounter++;
         }
         if (animals[i].equals(Animal.goat)){
            goatCounter++;
         }
      }
      //put sheep/goat into array
      for (int i = 0; i<animals.length; i++){
         if (i <= goatCounter - 1){
            animals[i] = Animal.goat;
         }else {
            animals[i] = Animal.sheep;
         }
      }

      //print out result
      System.out.println("number of goats: " + goatCounter);
      System.out.println("number of sheep: " + sheepCounter);
      for (int j = 0; j<animals.length; j++) {
         System.out.println(animals[j]);
      }
   }
}

